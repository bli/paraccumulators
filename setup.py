# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from setuptools import setup
from Cython.Build import cythonize

setup(
    name="paraccumulators",
    version="0.1",
    description="A way to consume tee'd generators in parallel",
    author="Blaise Li",
    author_email="blaise.li@normalesup.org",
    license="GNU GPLv3",
    packages=[
        "paraccumulators",],
    ext_modules = cythonize("paraccumulators/paraccumulators.pyx"),
    install_requires=["cytoolz"],
    zip_safe=False)
