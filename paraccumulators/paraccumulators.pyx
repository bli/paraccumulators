#!/usr/bin/env python3
# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
This module is mainly based on an answer to the following question:
http://stackoverflow.com/q/29508567/1878788

Credits go to http://stackoverflow.com/users/1405065/blckknght
"""

from itertools import tee
from cytoolz import accumulate, map, merge_with


def parallel_accumulators(input_generator, funcs, combiners):
    """
    This function creates a generator that will compute in parallel
    accumulators the results of some computations that can be described in
    terms of a fold (a.k.a. reduce or accumulate).

    The goal of the parallel computation is to avoid keeping in memory too many
    elements from the generator, while not having to initiate the generator
    once for each individual computation.

    Arguments:

    *input_generator* should yield the base elements on which the parallel
    computations will operate.

    *funcs* should be an iterable of one-argument functions,
    taking elements from *input_generator*. They convert these elements
    into a form suitable for their respective fold processing.

    *combiners* should be an iterable of two-arguments functions,
    taking elements of the same types as returned by the corresponding
    elements in *funcs*. They will perform the actual reductions during
    the folds.

    *funcs* and *combiners* should normally have the same number of elements.
    If one is longer than the others, the extra elements will be ignored.

    The resulting generator has to be iterated over:
    for (res1, res2, res3) in parallel_accumulators(
        input_generator,
        (func1, func2, func3),
        (combiner1, combiner2, combiner3)):
        pass

    The last yielded items correspond to the results of the computations.
    """
    processors = list(zip(funcs, combiners))
    iterators = tee(input_generator, len(processors))
    accumulators = []
    for (func, combiner), iterator in zip(processors, iterators):
        accumulators.append(accumulate(combiner, map(func, iterator)))
    return zip(*accumulators)


def add_dicts(*ds):
    """A cytoolz-based function that makes a sum of dictionaries."""
    return merge_with(sum, *ds)


def main():
    """Example use case."""

    def count_first(thing):
        return {thing[0]: 1}

    def count_last(thing):
        return {thing[-1]: 1}

    def identity(thing):
        return thing

    def concat_strings(*ss):
        return "\n".join(ss)
    from random import choice
    letters = "abcdefghijklmnopqrstuvwxyz"

    def random_strings(num_strings=100, string_len=20):
        for _ in range(num_strings):
            yield "".join(choice(letters) for _ in range(string_len))

    from itertools import repeat
    for firsts, lasts, text in parallel_accumulators(
            random_strings(),
            (count_first, count_last, identity),
            (*repeat(add_dicts, 2), concat_strings)):
        pass
    print("Text assembled from a stream of random strings")
    print(text)
    print("Counts of first letters obtained from the same initial stream")
    print(firsts)
    print("Counts of last letters obtained from the same initial stream")
    print(lasts)
    return 0


if __name__ == "__main__":
    import sys
    sys.exit(main())
