__copyright__ = "Copyright (C) 2020 Blaise Li"
__licence__ = "GNU GPLv3"
from .paraccumulators import parallel_accumulators
from .forkiter import FuncApplier, sum_tuples
#__all__ = ["parallel_accumulators", "add_dicts"]
